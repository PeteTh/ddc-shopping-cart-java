Shopping Cart application
=========================

Simple application to provide a service for buying a list of products by their item name. Service will calculate
if any of the products are currently on offer and apply offer discounts and provide the total costs incluuding discounts

Note: this requires your JAVA_HOME to be set to JDK8

build using:-
  mvn install