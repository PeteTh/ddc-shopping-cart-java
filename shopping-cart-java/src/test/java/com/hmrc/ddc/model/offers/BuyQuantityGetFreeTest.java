package com.hmrc.ddc.model.offers;

import org.junit.Test;

import static org.junit.Assert.*;

public class BuyQuantityGetFreeTest {

    Offer buy2Get1Free = new BuyQuantityGetFree("2 for 1", 2, -1);

    Offer buy3PayFor2 = new BuyQuantityGetFree("3 for 2", 3, -1);

    @Test
    public void testbuy2Get1Free_discountsBy1() throws Exception {
        assertEquals(-1, buy2Get1Free.applyOffer(2));
    }

    @Test
    public void testbuy2Get1Free_discountsBy0() throws Exception {
        assertEquals(0, buy2Get1Free.applyOffer(1));
    }

    @Test
    public void testbuy2Get1Free_discountsBy3() throws Exception {
        assertEquals(-3, buy2Get1Free.applyOffer(7));
    }

    @Test
    public void testbuy3PayFor2_discountsBy1() throws Exception {
        assertEquals(-1, buy3PayFor2.applyOffer(3));
    }

    @Test
    public void testbuy3PayFor2_discountsBy0() throws Exception {
        assertEquals(0, buy3PayFor2.applyOffer(2));
    }

    @Test
    public void testbuy3PayFor2_discountsBy3() throws Exception {
        assertEquals(-3, buy3PayFor2.applyOffer(10));
    }
}