package com.hmrc.ddc;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

/**
 *
 */
public class ShoppingServiceImplTest {
    private static final int DEFAULT_SCALE = 2;


    // NOTE: if had more time would add separate Unit Test for ShoppingCart class
    //        and then change this test to just test correct calls to ShoppingService are made

    ShoppingServiceImpl service = new ShoppingServiceImpl();

    @Test
    public void testCalculateTotalCostWhenNoOffersQualified() throws Exception {
        BigDecimal expected = new BigDecimal(0.85).setScale(DEFAULT_SCALE, BigDecimal.ROUND_HALF_UP);
        assertEquals(expected, service.calculateTotalCost("apple", "orange"));
    }

    @Test
    public void testCalculateTotalCostGivenTwoApplesOfferApplied() throws Exception {
        BigDecimal expected = new BigDecimal(0.85).setScale(DEFAULT_SCALE, BigDecimal.ROUND_HALF_UP);
        assertEquals(expected, service.calculateTotalCost("apple", "orange","apple"));
    }

    @Test
    public void testCalculateTotalCostGivenThreeOrangeOfferApplied() throws Exception {
        BigDecimal expected =   new BigDecimal(1.35).setScale(DEFAULT_SCALE, BigDecimal.ROUND_HALF_UP);
        assertEquals(expected, service.calculateTotalCost("orange","orange","orange","orange",
                "apple", "apple"));
    }

    @Test
    public void testCalculateTotalCostGivenMultipleOfferInstances() throws Exception {
        BigDecimal expected =   new BigDecimal(2.20).setScale(DEFAULT_SCALE, BigDecimal.ROUND_HALF_UP);
        assertEquals(expected, service.calculateTotalCost("orange","orange","orange","orange","orange","orange",
                "apple", "apple","apple", "apple"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testItemNotInProductCatalog() throws Exception {
        BigDecimal expected = new BigDecimal(0.85).setScale(DEFAULT_SCALE, BigDecimal.ROUND_HALF_UP);
        assertEquals(expected, service.calculateTotalCost("pear"));
    }

}