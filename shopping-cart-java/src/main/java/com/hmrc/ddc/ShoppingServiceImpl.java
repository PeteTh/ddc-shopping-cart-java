package com.hmrc.ddc;

import java.math.BigDecimal;

/**
 *
 */
public class ShoppingServiceImpl implements ShoppingService {


    public BigDecimal calculateTotalCost(String... items) {

        // Convert the list of bought Item names into a ShoppingCart of Products and Quantities
        ShoppingCart shoppingCart = new ShoppingCart(items);
        shoppingCart.applyOffers();
        shoppingCart.calculateTotalCost();

        return shoppingCart.getTotalCost();
    }



}
