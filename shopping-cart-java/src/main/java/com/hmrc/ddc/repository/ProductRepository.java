package com.hmrc.ddc.repository;

import com.hmrc.ddc.model.Product;

/**
 *
 */
public interface ProductRepository {

    Product getProduct(String item);
}
