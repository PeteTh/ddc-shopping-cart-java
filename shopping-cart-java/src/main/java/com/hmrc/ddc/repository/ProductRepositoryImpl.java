package com.hmrc.ddc.repository;

import com.hmrc.ddc.model.offers.BuyQuantityGetFree;
import com.hmrc.ddc.model.Product;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Simulates a repository of Products sold - normally would be from a persistent store
 */
public class ProductRepositoryImpl implements ProductRepository {

    private static final int DEFAULT_SCALE = 2;

    private static Map<String, Product> productCatalog = new HashMap<String, Product>();

    static {
        Product apple  = new Product("apple",  new BigDecimal(0.60).setScale(DEFAULT_SCALE, BigDecimal.ROUND_HALF_UP), new BuyQuantityGetFree("2 for 1", 2, -1));
        Product orange = new Product("orange", new BigDecimal(0.25).setScale(DEFAULT_SCALE, BigDecimal.ROUND_HALF_UP), new BuyQuantityGetFree("3 for 2", 3, -1));

        productCatalog.put(apple.getItemName(), apple);
        productCatalog.put(orange.getItemName(), orange);
    }


    public Product getProduct(String item) {
        return productCatalog.containsKey(item) ?  productCatalog.get(item) : null;
    }
}
