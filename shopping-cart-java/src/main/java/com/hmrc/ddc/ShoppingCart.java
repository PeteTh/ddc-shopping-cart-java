package com.hmrc.ddc;

import com.hmrc.ddc.model.Product;
import com.hmrc.ddc.repository.ProductRepository;
import com.hmrc.ddc.repository.ProductRepositoryImpl;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * Encapsulates the bought cart items in the form of: Product and Quantity bought.
 * Has the ability to apply offers and calculate total cost.
 */
public class ShoppingCart {

    // Key:Product    Value:Quantity bought
    private Map<Product, Integer> cartItems = new HashMap<Product, Integer>();

    private BigDecimal totalCost = BigDecimal.ZERO;

    private ProductRepository priceRepository = new ProductRepositoryImpl();


    /**
     * Construct a ShoppingCart instance from a list of itemNames the customer has bought.
     * @param itemNames e.g.   apple,orange,apple
     */
    public ShoppingCart(String... itemNames) {
        Arrays.stream(itemNames).forEach( (itemName) -> addCartItem(itemName));
    }

    /**
     * Adds a bought item to the ShoppingCart which holds unique Products and how many bought
     * @param itemName
     */
    public void addCartItem(String itemName) {
        Product product = priceRepository.getProduct(itemName);
        if (product == null) {
            throw new IllegalArgumentException("item not in product catalog");
        }

        // Do a calculation on each Map value's quantity
        cartItems.compute(product, (productKey, quantity) ->
            quantity == null ? 1 : quantity + 1
         );
    }

    /**
     * Each cartItem can be for a Product that is currently on Offer, apply the Offer which may result in a discounted Qty
     */
    public void applyOffers() {
        cartItems.forEach((product, quantityBought) -> {
            if (product.hasOffer()) {
                int quantityDiscount = product.getOffer().applyOffer(quantityBought);
                cartItems.replace(product, quantityBought + quantityDiscount);
            }
        });
    }

    /**
     * Price the Quantities - usually after the offers have been applied
     */
    public void calculateTotalCost() {
        totalCost = BigDecimal.ZERO;
        cartItems.forEach((product, quantity) -> {
            BigDecimal price = product.getPrice();
            totalCost = totalCost.add(price.multiply(BigDecimal.valueOf(quantity)));
        });
    }

    /**
     * Experimental calculateTotalCost using Java 8 stream API, but possibly too difficult to read.
     */
    public void calculateTotalCostUsingStream() {
        Function< Map.Entry<Product, Integer> , BigDecimal> itemCostFunction = cartItem -> {
            Product product = cartItem.getKey();
            Integer quantity = cartItem.getValue();
            return product.getPrice().multiply(BigDecimal.valueOf(quantity));
        };

        totalCost = cartItems.entrySet()
                .stream()
                .map(itemCostFunction)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public BigDecimal getTotalCost() {
        return totalCost;
    }
}
