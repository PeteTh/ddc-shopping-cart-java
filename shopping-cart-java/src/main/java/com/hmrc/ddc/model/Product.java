package com.hmrc.ddc.model;

import com.hmrc.ddc.model.offers.Offer;

import java.math.BigDecimal;

/**
 * Represents an Product Item that can be sold, which may have a related 'Offer'
 */
public class Product {

    private String itemName;
    private BigDecimal price;
    private Offer offer;

    public Product(String itemName, BigDecimal price, Offer offer) {
        this.itemName = itemName;
        this.price = price;
        this.offer = offer;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Offer getOffer() {
        return offer;
    }

    public boolean hasOffer() {
        return offer != null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Product product = (Product) o;

        if (!itemName.equals(product.itemName)) {
            return false;
        }
        return price != null ? price.equals(product.price) : product.price == null;

    }

    @Override
    public int hashCode() {
        return itemName.hashCode();
    }
}
