package com.hmrc.ddc.model.offers;

import com.hmrc.ddc.model.offers.Offer;

/**
 * Represents a Offer giving a quantity discount
 *
 * Future suggestions:  This could be enhanced to reference an OfferStrategy which different types of OfferType implement.
 *                       Then an OfferApplicator could apply offers but probably polymorphically
 */
public class BuyQuantityGetFree implements Offer {

    private String offerName;

    /* For 3 for the price of 2: this would be set to 3 */
    private int discountAtQuantity;

    /* For 3 for the price of 2: this would be set to -1 as the Qty is being discounted by 1 for every 3 that are bought */
    private int discountQuantityBy;

    public BuyQuantityGetFree(String offerName, int discountAtQuantity, int discountQuantityBy) {
        this.offerName = offerName;
        this.discountAtQuantity = discountAtQuantity;
        this.discountQuantityBy = discountQuantityBy;
    }


    @Override
    public String getOfferName() {
        return offerName;
    }

    public int getDiscountAtQuantity() {
        return discountAtQuantity;
    }

    public int getDiscountQuantityBy() {
        return discountQuantityBy;
    }

    /**
     * Applies an offer returning the amount to discount the quantity by
     * @param quantityBought
     * @return quantityDiscount
     */
    @Override
    public int applyOffer(Integer quantityBought) {
        int instancesOfOffer = quantityBought / discountAtQuantity;
        int quantityDiscount = instancesOfOffer * discountQuantityBy;
        return quantityDiscount;
    }
}
