package com.hmrc.ddc.model.offers;

/**
 * A Product Special Offer
 */
public interface Offer {
    String getOfferName();

    int applyOffer(Integer quantityBought);
}
