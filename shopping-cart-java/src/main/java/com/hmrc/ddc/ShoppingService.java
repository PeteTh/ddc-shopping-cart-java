package com.hmrc.ddc;

import com.hmrc.ddc.model.Money;

import java.math.BigDecimal;

public interface ShoppingService {

    /**
     * Given a list of items bought, calculates the total cost including any discount offers
     * @param items e.g.  "apple","orange","orange"
     * @return total cost
     */
    BigDecimal calculateTotalCost(String... items);
}
